/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.jpaexample;

import java.util.Date;
import ua.com.codefire.jpaexample.entity.User;
import ua.com.codefire.jpaexample.entity.Role;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import ua.com.codefire.jpaexample.entity.Article;
import ua.com.codefire.jpaexample.entity.Category;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("MainPU");

        // Session (begin)
        EntityManager manager = factory.createEntityManager();

        for (Article article : manager.createQuery("SELECT a FROM Article a", Article.class).getResultList()) {
            
            System.out.print(article.getTitle());
            
            System.out.print(" (");
            for (Category category : article.getCategoryList()) {
                System.out.print(" " + category.getName());
            }
            System.out.println(")");
        }
        
        Article article = new Article();
        article.setTitle("JAVA JPA Hibernate");
        article.setTimestamp(new Date());
        
        Category category = new Category();
        category.setName("Education");
        
        manager.getTransaction().begin();
        
        manager.persist(article);
        manager.persist(category);
        manager.flush();
        
        manager.refresh(article);
        
        article.getCategoryList().add(category);
        
        article = manager.merge(article);
        
        manager.getTransaction().commit();

        // Session (end)
        manager.close();

        factory.close();
    }
}
