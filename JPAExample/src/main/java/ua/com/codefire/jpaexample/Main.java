/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.jpaexample;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {
    
    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("MainPU");
        
        // Session (begin)
        EntityManager manager = factory.createEntityManager();
        
        // JPQL - Java Persistence Query Language
        TypedQuery<Page> query = manager.createQuery("SELECT p FROM Page p", Page.class);
        
        System.out.println(":: Pages list:");
        List<Page> pagesList = query.getResultList();
        
        for (Page page : pagesList) {
            System.out.println(page);
        }
        
        System.out.println(":: Page with #2:");
        Page indexPage = manager.find(Page.class, 2);
        
        System.out.println(indexPage);
        
        // Session (end)
        manager.close();
        
        factory.close();
    }
}
