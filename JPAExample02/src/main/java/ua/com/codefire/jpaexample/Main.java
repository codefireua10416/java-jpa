/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.jpaexample;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class Main {
    
    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("MainPU");
        
        // Session (begin)
        EntityManager manager = factory.createEntityManager();
        
        for (Role role : manager.createQuery("SELECT r FROM Role r", Role.class).getResultList()) {
            
            System.out.printf(":: %s\n", role);
            
            for (User user : role.getUserList()) {
                System.out.printf("  = %s\n", user);
            }
        }
        
        // Session (end)
        manager.close();
        
        factory.close();
    }
}
